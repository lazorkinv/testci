from django.contrib import admin
from django.urls import path, include
from lazoryk.views import CarViewSet, index
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register(r"cars", CarViewSet)
urlpatterns = [
    path('admin/', admin.site.urls),
    path("api/", include(router.urls))
]

