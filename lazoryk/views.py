from django.http import HttpResponse
from rest_framework import viewsets
from .models import Car
from .serializers import CarSerializer


class CarViewSet(viewsets.ModelViewSet):

    queryset = Car.objects.all()
    serializer_class = CarSerializer
    pagination_class = None

    def get_serializer_class(self):
        if self.request.method == "GET":
            return self.serializer_class
        return self.serializer_class


def index(request):
    return HttpResponse("👍")

