from django.apps import AppConfig


class LazorykConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lazoryk'
